import React, { useState } from "react";

const Interval = () => {
    const [intervals, setIntervals] = useState([]);

    const recordTime = () => {
        const currentTime = Date.now();
        setIntervals([...intervals, currentTime]);
    };

    const formatTime = (timestamp) => {
        const date = new Date(timestamp);
        const hours = date.getHours().toString().padStart(2, "0");
        const minutes = date.getMinutes().toString().padStart(2, "0");
        const seconds = date.getSeconds().toString().padStart(2, "0");
        const milliseconds = date.getMilliseconds().toString().padStart(3, "0");
        return `${hours}:${minutes}:${seconds}-${milliseconds}`;
    };

    return (
        <div style={{ textAlign: "center", marginTop: "20px" }}>
            <div style={{ fontSize: "26px" }}>w41</div>
            <button
                style={{ fontSize: "36px", marginTop: "20px"}}
                onClick={recordTime}
            >
                Interval time
            </button>
            <div>
                {intervals.map((time, index) => (
                    <div key={index} style={{ marginBottom: "5px" }}>
                        {formatTime(time)}
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Interval;
